import {Injectable} from '@angular/core';
import {QuestionModel} from './question.model';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuizService {
  quiz: QuestionModel[];

  quizUpdated = new Subject<QuestionModel[]>();

  constructor(private http: HttpClient) {
  }

  getQuizFromFile() {
    this.http.get<any>('../../assets/example2.json').subscribe(res => {
      console.log('res' + res.questions[0].question);
      this.quiz = res.questions;
      console.log(this.quiz);
      this.quizUpdated.next(<QuestionModel[]>[...this.quiz]);
    });
  }

  getUpdatedQuiz() {
    return this.quizUpdated.asObservable();
  }
}
