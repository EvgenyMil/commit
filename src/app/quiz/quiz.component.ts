import {Component, OnInit} from '@angular/core';
import {QuizService} from './quiz.service';
import {QuestionModel} from './question.model';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {

  quiz = [];
  currentQuestion: string[];
  favoriteSeason: string;
  seasons: string[] = ['Winter', 'Spring', 'Summer', 'Autumn'];

  constructor(private quizService: QuizService) {
    // sent it to mail: "igor.kharchenko@comm-it.com"
  }

  ngOnInit() {
    this.quizService.getQuizFromFile();
    this.quizService.quizUpdated.subscribe(quiz => {
      this.quiz = Array.from(quiz);
      console.log('Component', this.quiz);
      this.currentQuestion = this.quiz[0].options;
    });
  }


}
